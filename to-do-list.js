document.getElementById("addNewTask").addEventListener('click',addNewTask);
var ul =document.getElementById("task-list");
var input = document.getElementById("new-task");
var no =1;

input.onkeydown = function(e){
    if(e.keyCode == 13){
      addNewTask();
    }
 };

function addNewTask() {
    if(input.value==="")
    return;
    var p = document.createElement("p");
    var li = document.createElement("li");
    p.innerText=input.value;
    p.className = "contant";
    li.appendChild(getCheckBox());
    li.appendChild(p);
    li.appendChild(getSpan()).onclick = removeItem;
    li.className="item";
    li.id="item"+(no++);
    ul.appendChild(li);
}

function getCheckBox() {
    var checkBox = document.createElement("input");
    checkBox.type = "checkbox";
    checkBox.className = "checkbox";
    checkBox.onchange = completeTask;
    return checkBox;
}

function getSpan() {
    var span = document.createElement("span");
    span.innerText = "X";
    span.className = "removeBtn";
    return span;
}

var removeBtn = document.getElementsByClassName("removeBtn");
(Array.from(removeBtn)).forEach(function (a, b) {
    removeBtn[b].onclick = removeItem;
});
function removeItem() {
    this.parentElement.remove();
}

function completeTask() {
    if (this.checked) {
        this.nextSibling.className = "checked";
    } else {
        this.parentNode.childNodes[1].className= "unchecked";
    }
}

var draggedItem = document.getElementById("item0");
var dropLocation = document.getElementById("task-list");

draggedItem.ondragstart = function(evt){
    evt.preventDefault();
    evt.dataTransfer.setData('key',evt.target.id);
    console.log("1");
}

draggedItem.ondragover = function(evt){
    console.log("2");
    evt.preventDefault();
}

dropLocation.dragDrop =function(evt){
    console.log("3");
    evt.preventDefault();
    var dropItem = evt.dataTransfer.getData('key');
    var item = document.getElementById(dropItem);
    evt.target.appendChild(item);
}